# this can be updated to the latest:
#   pip install bumper
#   bump --file requirements.txt
#   commit the results
lxml==5.2.1
pandas==2.2.1
requests==2.31.0
selenium==4.19.0

