#!/usr/bin/env python3

"""
Python 3
    Web scraping using selenium to import a xml file
"""

from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select, WebDriverWait
from selenium.common.exceptions import TimeoutException, NoSuchElementException
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.chrome.options import Options as ChromeOptions

import time
import sys
import os


options = FirefoxOptions()
# options.browser_version = '92'
# options.platform_name = 'Windows 10'
driver = webdriver.Remote(
        'http://selenium-standalone-firefox:4444/wd/hub',
        options=options)

#driver = webdriver.Firefox()

def archivist_login(url, uname, pw):
    """
    Log in to archivist
    """
    driver.get(url)
    time.sleep(5)
    driver.find_element(By.ID, "email").send_keys(uname)
    driver.find_element(By.ID, "password").send_keys(pw)
    driver.find_element(By.XPATH, '//span[text()="Log In"]').click()
    time.sleep(5)


def upload_check(xmlFiles, log_dir):
    """
    Check the log for the uploaded instrument File
    """
    success_dir = os.path.join(log_dir, 'success')
    if not os.path.exists(success_dir):
        os.makedirs(success_dir)

    failure_dir = os.path.join(log_dir, 'failure')
    if not os.path.exists(failure_dir):
        os.makedirs(failure_dir)

    # Log in
    import_url = "https://closer-build.herokuapp.com/admin/imports/"
    driver.get(import_url)
    time.sleep(5)

    with open(os.path.join(log_dir, 'summary.csv'), "a") as f:
        f.write( ",".join(["File", "State", "Date"]) + "\n")

    # Check if uploaded correctly
    for i in range(len(xmlFiles)):

        # choose from dropdown to display all results on one page
        select = Select(driver.find_element(By.XPATH, "//select[@aria-label='rows per page']"))

        # select by visible text
        select.select_by_visible_text('All')

        tr = driver.find_elements(By.XPATH, "html/body/div/div/div/div/main/div/div/div/div/table/tbody/tr")[i]
        file_row = tr.find_elements(By.XPATH, "td")
        filename = file_row[1].text
        status = file_row[3].text
        print(i, filename, status)

        with open(os.path.join(log_dir, 'summary.csv'), "a") as f:
            f.write( ",".join([filename, status, file_row[4].text]) + "\n")

        log_url = tr.find_element(By.XPATH, "td[6]/button/span/a").get_attribute("href")
        driver.get(log_url)
        time.sleep(5)

        if status == 'failure':
            with open(os.path.join(failure_dir, filename + '_log.html'), 'w') as f:
                f.write(driver.page_source)
        elif status == 'success':
            with open(os.path.join(success_dir, filename + '_log.html'), 'w') as f:
                f.write(driver.page_source)
        else:
            with open(os.path.join(log_dir, filename + '_log.html'), 'w') as f:
                f.write(driver.page_source)

        driver.back()


def main():
    uname = sys.argv[1]
    pw = sys.argv[2]

    # log in
    archivist_url = "https://closer-build.herokuapp.com"
    archivist_login(archivist_url, uname, pw)

    main_dir = 'input'
    input_names = [f for f in os.listdir(main_dir) if f.endswith('.xml')]
    number_input = len(input_names)

    log_dir = 'log'
    if not os.path.exists(log_dir):
        os.makedirs(log_dir)

    xmlFiles = [os.path.join(main_dir, input_name) for input_name in input_names]

    upload_check(xmlFiles, log_dir)

    driver.quit()


if __name__ == "__main__":
    main()

