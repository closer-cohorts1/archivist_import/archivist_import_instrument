#!/usr/bin/env python3

"""
Python 3
    Web scraping using selenium to import a xml file
"""

from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select, WebDriverWait
from selenium.common.exceptions import TimeoutException, NoSuchElementException
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.chrome.options import Options as ChromeOptions

import time
import sys
import os


options = FirefoxOptions()
# options.browser_version = '92'
# options.platform_name = 'Windows 10'
driver = webdriver.Remote(
        'http://selenium-standalone-firefox:4444/wd/hub',
        options=options)

#driver = webdriver.Firefox()

def archivist_login(url, uname, pw):
    """
    Log in to archivist
    """
    driver.get(url)
    time.sleep(5)
    driver.find_element(By.ID, "email").send_keys(uname)
    driver.find_element(By.ID, "password").send_keys(pw)
    driver.find_element(By.XPATH, '//span[text()="Log In"]').click()
    time.sleep(5)


def upload_instrument(xmlFile):
    """
    Upload DDI Instrument File
    """
    # Upload page
    import_url = "https://closer-build.herokuapp.com/admin/import"
    driver.get(import_url)
    time.sleep(5)

    # Upload DDI Instrument Files
    # Identify element
    s = driver.find_elements(By.XPATH, "//input[@type='file']")[0]
    # File path specified with send_keys
    s.send_keys(os.path.abspath(xmlFile))
    # Import Instrument button
    importButton = driver.find_element(By.XPATH, "//*[contains(text(), 'Import Instrument')]")
    importButton.click()
    try:
        # wait until it is uploaded
        CreatedElement = WebDriverWait(driver, 20).until(EC.presence_of_element_located((By.XPATH,  "//*[contains(text(), 'Created')]")))
        print(xmlFile + " created!")
    except TimeoutException:
        print("uploading took too much time!")


def main():
    uname = sys.argv[1]
    pw = sys.argv[2]

    # log in
    # archivist_url = "https://closer-build.herokuapp.com"
    archivist_url = "https://closer-archivist-staging.herokuapp.com/"
    archivist_login(archivist_url, uname, pw)

    main_dir = 'input'
    input_names = [f for f in os.listdir(main_dir) if f.endswith('.xml')]
    number_input = len(input_names)

    log_dir = 'log'
    if not os.path.exists(log_dir):
        os.makedirs(log_dir)

    xmlFiles = [os.path.join(main_dir, input_name) for input_name in input_names]

    print(xmlFiles)
    for xmlFile in xmlFiles:
        print(xmlFile)
        upload_instrument(xmlFile)

    driver.quit()


if __name__ == "__main__":
    main()

